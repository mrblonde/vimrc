set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Vundle
Plugin 'VundleVim/Vundle.vim'

" clang
Plugin 'rhysd/vim-clang-format'

" whitespace
Plugin 'bronson/vim-trailing-whitespace'

" cscope
Plugin 'chazy/cscope_maps'

" git highlight
Plugin 'airblade/vim-gitgutter'

" local vimrc
Plugin 'embear/vim-localvimrc'

" Rust language
Plugin 'rust-lang/rust.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

syntax on
set number
set colorcolumn=100
highlight ColorColumn ctermbg=darkgray

set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" " On pressing tab, insert 4 spaces
set expandtab
set wildmenu
set wildmode=longest,list,full

nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

let g:localvimrc_ask=0
